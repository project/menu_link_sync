# Menu Link Sync

The Menu Link Sync module helps synchronize the position of menu items within
Drupal installs with separate menus for every language, for instance when we
have different "Main Menu - French", "Main Menu - English" and
"Main Menu - Spanish" menus.

This synchronization may be useful when the structure of the menus is too
different for the built-in menu translation to be an adequate solution, but when
the trees for different languages are still similar enough for some sort of
synchronization of menu structures to be desirable.

This module provides a "Synchronize" button to the "Menu link settings" on the
node forms for translated nodes. By pressing the "Synchronize" button, the
parent and the relative tree position of the menu link for this translated
node are automatically calculated to be as close as possible to the parent
and relative position of the menu link for the default translation of the node.
The form will then be updated through an AJAX call and the new parent and
weight will be automatically selected.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/menu_link_sync).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/menu_link_sync).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

This module requires [Menu Link Weight](https://www.drupal.org/project/menu_link_weight)

This integration allows us to synchronize the relative position of a menu link
within a menu tree, instead of simply copying the absolute numeric weight of the
menu link from the source node.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No special configuration is needed.


## Maintainers

- Bart Hanssens - [bart.hanssens](https://www.drupal.org/u/barthanssens)
- Ruben Marques - [rutiolma](https://www.drupal.org/u/rutiolma)
- Stefan Ruijsenaars - [stefan.r](https://www.drupal.org/u/stefanr-0)
- [Ludo.R](https://www.drupal.org/u/ludor)
- [gyd](https://www.drupal.org/u/gyd)
